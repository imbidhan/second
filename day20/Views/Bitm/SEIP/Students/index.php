<?php

include_once("../../../../vendor/autoload.php");
include_once("../../../../vendor/mpdf/mpdf/mpdf.php");

use App\Bitm\SEIP\Students\Students;

?>
<html>
<head>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <title>View informaiton</title>
</head>

<body>
<form action="search.php" method="POST">
    <table>
        <tr>
            <td><input type="text" name="search" placeholder="Search item"/></td>
            <td><input type="submit" name="form" value="Search"/></td>
        </tr>

    </table>
</form>
<div>
    <span>Search/Filter</span>
    <span id="utility">Download as <a href="pdf.php">PDF</a> | <a href="xl.php">XL</a> |<a href="create.php">Create</a> </span>
    <form action="index.php">
        <select name="itemPerPage">
            <option value="10">10</option>
            <option value="20">20</option>
            <option value="30">30</option>
            <option value="40">40</option>
            <option value="50">50</option>
        </select>
    </form>
</div>
<table border="1">
    <thead>List of Students</thead>
    <tr>
        <td>ID</td>
        <td>Title</td>
        <td>Action</td>
    </tr>

    <?php

    $obj = new Students();

    $data = $obj->index();

    foreach ($data as $key => $value) {
        ?>
        <tr>
            <td><?php echo $key + 1; ?> </td>
            <td><?php echo $value['title']; ?></td>
            <td>
                <a href="show.php?id=<?php echo $value['id'];?>">View</a> |
                <a href="delete.php?id=<?php echo $value['id'];?>">Delete</a></td>
        </tr>
    <?php } ?>
</table>
</body

</html>


