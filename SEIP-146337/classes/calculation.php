<?php
class Calculation{
	public $a = "";
	public $b = "";
	public $result;
	public function getValue($x,$y){
		$this->a = $x;
		$this->b = $y;
		return $this;
	}
	public function Sum(){
		echo "Summation is : ".($this->a+$this->b)."<br/>";
		return $this;
	}
	public function Sub(){
		echo "Subtraction is : ".($this->a - $this->b)."<br/>";
		return $this;
	}
	public function Mul(){
		echo "Multiplication is : ".($this->a * $this->b)."<br/>";
		return $this;
	}
	public function Division(){
		echo "Division is : ".($this->a / $this->b)."<br/>";
		return $this;
	}
}