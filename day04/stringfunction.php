<?php
/**
 * Created by PhpStorm.
 * User: Web App Develop PHP
 * Date: 12/17/2016
 * Time: 9:17 AM
 */
//addcslashes function
echo "addcslashes function()<br>";
$str = "Welcome to my humble Homepage!";
echo $str."<br>";
echo addcslashes($str,'m')."<br>";
echo addcslashes($str,'H')."<br>";

echo "</br>";
echo "</br>";
echo "</br>";
//addslashes funciton

echo "Addslashes funciton()<br>";
$str = "Who's Peter Griffin?";
echo $str . " This is not safe in a database query.<br>";
echo addslashes($str) . " This is safe in a database query.";

echo "<br>";
echo "<br>";
echo "<br>";
echo "Explode funcition()<br>";
$str1 = 'one,two,three,four';

// zero limit
print_r(explode(',',$str1,0));
print "<br>";

// positive limit
print_r(explode(',',$str1,2));
print "<br>";

// negative limit
print_r(explode(',',$str1,-1));

echo "<br>";
echo "<br>";
echo "<br>";

echo "Implode function () <br>";
$arr = array('Hello','World!','Beautiful','Day!');
echo implode(" ",$arr)."<br>";
echo implode("+",$arr)."<br>";
echo implode("-",$arr)."<br>";
echo implode("X",$arr);
echo "<br>";
echo "<br>";
echo "<br>";

echo "md5 function ()";
$str = "Hello";
echo "The string: ".$str."<br>";
echo "TRUE - Raw 16 character binary format: ".md5($str, TRUE)."<br>";
echo "FALSE - 32 character hex number: ".md5($str)."<br>";
echo "<br>";
echo "<br>";
echo "<br>";

echo "n12bn function () <br>";
echo nl2br("One line.\nAnother line.",false);
echo "<br>";
echo "<br>";
echo "<br>";

echo "str_pad function () <br>";
$str2 = "Hello World";
echo str_pad($str2,20,".");
echo "<br>";
echo "<br>";
echo "<br>";

echo "str_repeat function () <br>";
echo str_repeat(".",13);
echo "<br>";
echo "<br>";
echo "<br>";

echo "str_replace function () <br>";
echo str_replace("world","Peter","Hello world!");
echo "<br>";
echo "<br>";
echo "<br>";

echo "shuffle function ()<br>";
echo str_shuffle("Hello World");
echo "<br>Try to refresh the page.
This function will randomly shuffle all characters each time.<br>";
echo "<br>";
echo "<br>";
echo "<br>";

echo "str_sloit function ()";
print_r(str_split("Hello",3));
echo "<br>";
echo "<br>";
echo "<br>";

echo "str_word_count funtion () <br>";
echo str_word_count("Hello world!");
echo "<br>";
echo "<br>";
echo "<br>";

echo "strlen function () <br>";
echo strlen("Hello world!");
echo "<br>Whitespaces and exclamation m
arks are also a part of the string 'HelloWorld' would return 10).";
echo "<br>";
echo "<br>";
echo "<br>";

echo "strtolower function ()<br>";
echo strtolower("Hello WORLD.");
echo "<br>";
echo "<br>";
echo "<br>";

echo "strtoupper function ()<br>";
echo strtoupper("Bidhan sutradhar");
echo "<br>";
echo "<br>";
echo "<br>";

echo "ucfirst function ()<br>";
echo ucfirst("apoun mix");
echo "<br>";
echo "<br>";
echo "<br>";

echo "ucwords function () <br>";
echo ucwords("kanchan pur high school");
echo "<br>";
echo "<br>";
echo "<br>";

