<?php
error_reporting(E_ALL ^ E_NOTICE);
$f_num = $_POST['num1'];
$s_num = $_POST['num2'];

$sum = $_POST['sum'];
$sub = $_POST['sub'];
$mul = $_POST['mul'];
$div = $_POST['div'];
$modulas = $_POST['modulas'];

function calculation($f_num,$s_num,$sum,$sub,$mul,$div,$modulas){
	if (isset($sum)) {
		if (empty($f_num)) {
			echo "First number can't be empty.";
		}elseif (empty($s_num)) {
			echo "Second number can't be empty.";
		}elseif (!is_numeric($f_num) || !is_numeric($s_num)) {
			echo "Input data must be numeric value.";
		}else{
			echo "The summation is : ".$sum = $f_num + $s_num;
		}
	}elseif (isset($sub)) {
		if (empty($f_num)) {
			echo "First number can't be empty.";
		}elseif (empty($s_num)) {
			echo "Second number can't be empty.";
		}elseif (!is_numeric($f_num) || !is_numeric($s_num)) {
			echo "Input data must be numeric value.";
		}else{
			echo "The subtraction is : ".$sub = $f_num - $s_num;
		}
	}elseif (isset($mul)) {
		if (empty($f_num)) {
			echo "First number can't be empty.";
		}elseif (empty($s_num)) {
			echo "Second number can't be empty.";
		}elseif (!is_numeric($f_num) || !is_numeric($s_num)) {
			echo "Input data must be numeric value.";
		}else{
			echo "The Multiplai is : ".$mul = $f_num * $s_num;
		}
	}elseif (isset($div)) {
		if (empty($f_num)) {
			echo "First number can't be empty.";
		}elseif (empty($s_num)) {
			echo "Second number can't be empty.";
		}elseif (!is_numeric($f_num) || !is_numeric($s_num)) {
			echo "Input data must be numeric value.";
		}else{
			echo "The division is : ".$div = $f_num / $s_num;
		}
	}elseif (isset($modulas)) {
		if (empty($f_num)) {
			echo "First number can't be empty.";
		}elseif (empty($s_num)) {
			echo "Second number can't be empty.";
		}elseif (!is_numeric($f_num) || !is_numeric($s_num)) {
			echo "Input data must be numeric value.";
		}else{
			echo "The modulas is : ".$modulas = $f_num % $s_num;
		}
	}
}
calculation($f_num,$s_num,$sum,$sub,$mul,$div,$modulas);
