<?php
$name   = $_POST['name'];
$mark   = $_POST['mark'];
$submit = $_POST['submit'];

function result($name,$mark,$submit){
    if(isset($submit)){
        if(empty($name) || empty($mark)){
            echo "Input field can't be empty.";
        }elseif(!is_numeric($mark)){
            echo "Mark field must be integer type";
        }else{
            if($mark<40) {
                echo $name . "<br/>";
                echo "Result : F <br/>";
            }elseif($mark>=40 && $mark<60){
                echo $name . "<br/>";
                echo "Result : B <br/>";
            }elseif($mark>=60 && $mark<75){
                echo $name . "<br/>";
                echo "Result : C <br/>";
            }elseif($mark>=75 && $mark<80){
                echo $name . "<br/>";
                echo "Result : B <br/>";
            }elseif($mark>=80 && $mark<83){
                echo $name . "<br/>";
                echo "Result : B+ <br/>";
            }elseif($mark>=83 && $mark<87){
                echo $name . "<br/>";
                echo "Result : A <br/>";
            }elseif($mark>=87 && $mark<90){
                echo $name . "<br/>";
                echo "Result : A+ <br/>";
            }elseif($mark>=90 && $mark<101){
                echo $name . "<br/>";
                echo "Result : Golden A+ <br/>";
            }else{
                echo "Input mark is too high.input takes between 1 to 100";
            }
        }
    }
}
result($name,$mark,$submit);