<?php
class Calculator{
    public $a = '';
    public $b = '';
    public function getValue($x,$y){
        $this->a = $x;
        $this->b = $y;
        return $this;
    }
    public function summation(){
        echo "The summation is : ".($this->a + $this->b)."<br/>";
        return $this;
    }
    public function subtraction(){
        echo "The subtraction is : ".($this->a - $this->b)."<br/>";
        return $this;
    }
    public function Multiply(){
        echo "The Multiply is : ".($this->a * $this->b)."<br/>";
        return $this;
    }
    public function Division(){
        echo "The Division is : ".($this->a / $this->b)."<br/>";
        return $this;
    }
}