<?php
/**
 * Created by PhpStorm.
 * User: Web App Develop PHP
 * Date: 12/14/2016
 * Time: 10:32 AM
 */
$a = 10.55;
if(is_float($a)){
 echo "a is float data";
}else{
    echo "a is not float data";
}

echo "<br>";

$b = "Bidhan sutradhar";
if(is_array($b)){
 echo "b is array";
}else{
    echo "B is not array";
}

echo "<br>";
$c = null;
if(is_null($c)){
 echo "c is null";
}else{
    echo "c is not null";
}

echo "<br>";
$d = true;
if(is_bool($d)){
 echo "D is a bool value";
}else{
    echo "D is not bool value";
}

echo "<br>";
$e = array("Bangladesh");
//$e = "Bangladesh";
echo serialize($e);
echo "<br>";
//$f = array("kanchanpur", "hight");
//echo unserialize($f);
echo "<br>";

$x = 20;
echo $x;
unset($x);
echo $x;

echo "<br>";

$ar = array("bidhan","sutradhar","kanchanpur");
echo "<pre>";
 print_r($ar);
echo "</pre>";

